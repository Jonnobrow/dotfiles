#!/bin/sh
ZSH_USERS="https://github.com/zsh-users"
PLUGINS=("zsh-history-substring-search" "zsh-syntax-highlighting" "zsh-autosuggestions")
BASE_DIR="$HOME/.local/share/zsh"

for p in "${PLUGINS[@]}"; do
	if [[ -d "$BASE_DIR/$p" ]]; then
		cd "$BASE_DIR/$p"
		git pull
	else
		git clone "$ZSH_USERS/$p.git" "$BASE_DIR/$p"
	fi
done
