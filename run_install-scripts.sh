#!/bin/sh

for script in $HOME/.config/scripts/*; do
	basepath="$HOME/.local/bin/$(basename $script)"
	if [[ -L $basepath ]]; then
		rm $basepath
	fi
	ln -s $script $HOME/.local/bin
done
echo "Cannot install scripts on {{ .chezmoi.os }}"
