(setq user-full-name "Jonathan Bartlett"
      user-mail-address "jonathan@jonnobrow.co.uk")

(setq doom-theme 'ewal-doom-one
      doom-font (font-spec :family "Source Code Pro" :size 12)
      display-line-numbers-type t)

(display-time-mode 1)
(display-battery-mode 1)
    (toggle-frame-maximized)

(remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-shortmenu)

(add-hook! 'org-mode-hook
           (lambda () (org-bullets-mode 1)))
(after! org (setq org-hide-emphasis-markers t
                  org-hide-leading-stars t
                  org-ellipsis " ▼ "
                  org-bullets-bullet-list '("○" "☉" "◎" "◉" "○" "◌" "◎" "●" "◦")))

(add-hook! 'org-babel-after-execute-hook 'org-display-inline-images 'append)

(after! org (setq org-directory "~/cloud/org"
                  org-preview-latex-image-directory "/tmp/ltximg"
                  org-src-window-setup 'current-window
                  projectile-project-search-path "~/local/projects/"))

(after! org
  (setq org-use-fast-todo-selection t)
  (setq org-todo-keywords (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
                                  (sequence "WAITING(w/@!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)"))))
  (setq org-todo-keyword-faces
        (quote (("TODO" :foreground "red" :weight bold)
                ("NEXT" :foreground "sky blue" :weight bold)
                ("DONE" :foreground "chartreuse" :weight bold)
                ("WAITING" :foreground "dark orange" :weight bold)
                ("HOLD" :foreground "light coral" :weight bold)
                ("CANCELLED" :foreground "dark green" :weight bold))))

  (setq org-todo-state-tags-triggers
        (quote (("CANCELLED" ("CANCELLED" . t))
                ("WAITING" ("WAITING" . t))
                ("HOLD" ("WAITING") ("HOLD" . t)) ;; When we mark as HOLD, add HOLD and remove WAITING
                (done ("WAITING") ("HOLD"))       ;; Remove WAITING and HOLD
                ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
                ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
                ("DONE" ("WAITING") ("CANCELLED") ("HOLD"))
                ))))

(after! org
  (setq org-capture-templates
        (quote (("t" "todo" entry (file "~/cloud/org/refile.org")
                 "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
                ("n" "note" entry (file "~/cloud/org/refile.org")
                 "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
                ("j" "journal" entry (file+datetree "~/cloud/org/diary.org")
                 "* %?\n%U\n" :clock-in t :clock-resume t)
                ("l" "logbook" entry (file+datetree "~/cloud/org/work/LOGBOOK.org")
                 "* %?\n%U\n" :clock-in t :clock-resume t)))))

; Exclude DONE state tasks from refile targets
(defun bh/verify-refile-target ()
  "Exclude todo keywords with a done state from refile targets"
  (not (member (nth 2 (org-heading-components)) org-done-keywords)))

(after! org
  (setq org-refile-targets (quote ((nil :maxlevel . 9) ;; nil means current file
                                   (org-agenda-files :maxlevel . 9))))
  (setq org-refile-use-outline-path t)
  (setq org-outline-path-complete-in-steps nil)
  (setq org-refile-allow-creating-parent-nodes (quote confirm))
  (setq org-refile-target-verify-function 'bh/verify-refile-target))

(after! org
  (setq org-agenda-dim-blocked-tasks nil
        org-agenda-sticky t
        org-agenda-inhibit-startup nil
        org-agenda-compact-blocks nil
        org-deadline-warning-days 30 ;; Give me a months notice about deadlines
        org-agenda-tags-todo-honor-ignore-options t
        org-agenda-files (quote ("~/cloud/org/home"
                                 "~/cloud/org/work"
                                 "~/cloud/org/archive"
                                 "~/cloud/org"))))

(defun bh/is-project-p ()
  "Any task with a todo keyword subtask"
  (save-restriction
    (widen)
    (let ((has-subtask)
          (subtree-end (save-excursion (org-end-of-subtree t)))
          (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
      (save-excursion
        (forward-line 1)
        (while (and (not has-subtask)
                    (< (point) subtree-end)
                    (re-search-forward "^\*+ " subtree-end t))
          (when (member (org-get-todo-state) org-todo-keywords-1)
            (setq has-subtask t))))
      (and is-a-task has-subtask))))

(defun bh/is-project-subtree-p ()
  "Any task with a todo keyword that is in a project subtree.
Callers of this function already widen the buffer view."
  (let ((task (save-excursion (org-back-to-heading 'invisible-ok)
                              (point))))
    (save-excursion
      (bh/find-project-task)
      (if (equal (point) task)
          nil
        t))))

(defun bh/is-task-p ()
  "Any task with a todo keyword and no subtask"
  (save-restriction
    (widen)
    (let ((has-subtask)
          (subtree-end (save-excursion (org-end-of-subtree t)))
          (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
      (save-excursion
        (forward-line 1)
        (while (and (not has-subtask)
                    (< (point) subtree-end)
                    (re-search-forward "^\*+ " subtree-end t))
          (when (member (org-get-todo-state) org-todo-keywords-1)
            (setq has-subtask t))))
      (and is-a-task (not has-subtask)))))

(defun bh/is-subproject-p ()
  "Any task which is a subtask of another project"
  (let ((is-subproject)
        (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
    (save-excursion
      (while (and (not is-subproject) (org-up-heading-safe))
        (when (member (nth 2 (org-heading-components)) org-todo-keywords-1)
          (setq is-subproject t))))
    (and is-a-task is-subproject)))

(defun bh/list-sublevels-for-projects-indented ()
  "Set org-tags-match-list-sublevels so when restricted to a subtree we list all subtasks.
  This is normally used by skipping functions where this variable is already local to the agenda."
  (if (marker-buffer org-agenda-restrict-begin)
      (setq org-tags-match-list-sublevels 'indented)
    (setq org-tags-match-list-sublevels nil))
  nil)

(defun bh/list-sublevels-for-projects ()
  "Set org-tags-match-list-sublevels so when restricted to a subtree we list all subtasks.
  This is normally used by skipping functions where this variable is already local to the agenda."
  (if (marker-buffer org-agenda-restrict-begin)
      (setq org-tags-match-list-sublevels t)
    (setq org-tags-match-list-sublevels nil))
  nil)

(defvar bh/hide-scheduled-and-waiting-next-tasks t)

(defun bh/toggle-next-task-display ()
  (interactive)
  (setq bh/hide-scheduled-and-waiting-next-tasks (not bh/hide-scheduled-and-waiting-next-tasks))
  (when  (equal major-mode 'org-agenda-mode)
    (org-agenda-redo))
  (message "%s WAITING and SCHEDULED NEXT Tasks" (if bh/hide-scheduled-and-waiting-next-tasks "Hide" "Show")))

(defun bh/skip-stuck-projects ()
  "Skip trees that are not stuck projects"
  (save-restriction
    (widen)
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (if (bh/is-project-p)
          (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
                 (has-next ))
            (save-excursion
              (forward-line 1)
              (while (and (not has-next) (< (point) subtree-end) (re-search-forward "^\\*+ NEXT " subtree-end t))
                (unless (member "WAITING" (org-get-tags-at))
                  (setq has-next t))))
            (if has-next
                nil
              next-headline)) ; a stuck project, has subtasks but no next task
        nil))))

(defun bh/skip-non-stuck-projects ()
  "Skip trees that are not stuck projects"
  ;; (bh/list-sublevels-for-projects-indented)
  (save-restriction
    (widen)
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (if (bh/is-project-p)
          (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
                 (has-next ))
            (save-excursion
              (forward-line 1)
              (while (and (not has-next) (< (point) subtree-end) (re-search-forward "^\\*+ NEXT " subtree-end t))
                (unless (member "WAITING" (org-get-tags-at))
                  (setq has-next t))))
            (if has-next next-headline nil)) ; a stuck project, has subtasks but no next task
        next-headline))))

(defun bh/skip-non-projects ()
  "Skip trees that are not projects"
  ;; (bh/list-sublevels-for-projects-indented)
  (if (save-excursion (bh/skip-non-stuck-projects))
      (save-restriction
        (widen)
        (let ((subtree-end (save-excursion (org-end-of-subtree t))))
          (cond
           ((bh/is-project-p)
            nil)
           ((and (bh/is-project-subtree-p) (not (bh/is-task-p)))
            nil)
           (t
            subtree-end))))
    (save-excursion (org-end-of-subtree t))))

(defun bh/skip-non-tasks ()
  "Show non-project tasks.
Skip project and sub-project tasks, habits, and project related tasks."
  (save-restriction
    (widen)
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (cond
       ((bh/is-task-p)
        nil)
       (t
        next-headline)))))

(defun bh/skip-project-trees-and-habits ()
  "Skip trees that are projects"
  (save-restriction
    (widen)
    (let ((subtree-end (save-excursion (org-end-of-subtree t))))
      (cond
       ((bh/is-project-p)
        subtree-end)
       (t
        nil)))))

(defun bh/skip-projects-and-habits-and-single-tasks ()
  "Skip trees that are projects, tasks that are habits, single non-project tasks"
  (save-restriction
    (widen)
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (cond
       ((and bh/hide-scheduled-and-waiting-next-tasks
             (member "WAITING" (org-get-tags-at)))
        next-headline)
       ((bh/is-project-p)
        next-headline)
       ((and (bh/is-task-p) (not (bh/is-project-subtree-p)))
        next-headline)
       (t
        nil)))))

(defun bh/skip-project-tasks-maybe ()
  "Show tasks related to the current restriction.
When restricted to a project, skip project and sub project tasks, habits, NEXT tasks, and loose tasks.
When not restricted, skip project and sub-project tasks, habits, and project related tasks."
  (save-restriction
    (widen)
    (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
           (next-headline (save-excursion (or (outline-next-heading) (point-max))))
           (limit-to-project (marker-buffer org-agenda-restrict-begin)))
      (cond
       ((bh/is-project-p)
        next-headline)
       ((and (not limit-to-project)
             (bh/is-project-subtree-p))
        subtree-end)
       ((and limit-to-project
             (bh/is-project-subtree-p)
             (member (org-get-todo-state) (list "NEXT")))
        subtree-end)
       (t
        nil)))))

(defun bh/skip-project-tasks ()
  "Show non-project tasks.
Skip project and sub-project tasks, habits, and project related tasks."
  (save-restriction
    (widen)
    (let* ((subtree-end (save-excursion (org-end-of-subtree t))))
      (cond
       ((bh/is-project-p)
        subtree-end)
       ((bh/is-project-subtree-p)
        subtree-end)
       (t
        nil)))))

(defun bh/skip-non-project-tasks ()
  "Show project tasks.
Skip project and sub-project tasks, habits, and loose non-project tasks."
  (save-restriction
    (widen)
    (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
           (next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (cond
       ((bh/is-project-p)
        next-headline)
       ((and (bh/is-project-subtree-p)
             (member (org-get-todo-state) (list "NEXT")))
        subtree-end)
       ((not (bh/is-project-subtree-p))
        subtree-end)
       (t
        nil)))))

(defun bh/skip-projects-and-habits ()
  "Skip trees that are projects and tasks that are habits"
  (save-restriction
    (widen)
    (let ((subtree-end (save-excursion (org-end-of-subtree t))))
      (cond
       ((bh/is-project-p)
        subtree-end)
       (t
        nil)))))

(defun bh/skip-non-subprojects ()
  "Skip trees that are not projects"
  (let ((next-headline (save-excursion (outline-next-heading))))
    (if (bh/is-subproject-p)
        nil
      next-headline)))

(setq bh/keep-clock-running nil)

(defun bh/clock-in-to-next (kw)
  "Switch a task from TODO to NEXT when clocking in.
Skips capture tasks, projects, and subprojects.
Switch projects and subprojects from NEXT back to TODO"
  (when (not (and (boundp 'org-capture-mode) org-capture-mode))
    (cond
     ((and (member (org-get-todo-state) (list "TODO"))
           (bh/is-task-p))
      "NEXT")
     ((and (member (org-get-todo-state) (list "NEXT"))
           (bh/is-project-p))
      "TODO"))))

(defun bh/find-project-task ()
  "Move point to the parent (project) task if any"
  (save-restriction
    (widen)
    (let ((parent-task (save-excursion (org-back-to-heading 'invisible-ok) (point))))
      (while (org-up-heading-safe)
        (when (member (nth 2 (org-heading-components)) org-todo-keywords-1)
          (setq parent-task (point))))
      (goto-char parent-task)
      parent-task)))

(defun bh/punch-in (arg)
  "Start continuous clocking and set the default task to the
selected task.  If no task is selected set the Organization task
as the default task."
  (interactive "p")
  (setq bh/keep-clock-running t)
  (if (equal major-mode 'org-agenda-mode)
      ;;
      ;; We're in the agenda
      ;;
      (let* ((marker (org-get-at-bol 'org-hd-marker))
             (tags (org-with-point-at marker (org-get-tags-at))))
        (if (and (eq arg 4) tags)
            (org-agenda-clock-in '(16))
          (bh/clock-in-organization-task-as-default)))
    ;;
    ;; We are not in the agenda
    ;;
    (save-restriction
      (widen)
      ; Find the tags on the current task
      (if (and (equal major-mode 'org-mode) (not (org-before-first-heading-p)) (eq arg 4))
          (org-clock-in '(16))
        (bh/clock-in-organization-task-as-default)))))

(defun bh/punch-out ()
  (interactive)
  (setq bh/keep-clock-running nil)
  (when (org-clock-is-active)
    (org-clock-out))
  (org-agenda-remove-restriction-lock))

(defun bh/clock-in-default-task ()
  (save-excursion
    (org-with-point-at org-clock-default-task
      (org-clock-in))))

(defun bh/clock-in-parent-task ()
  "Move point to the parent (project) task if any and clock in"
  (let ((parent-task))
    (save-excursion
      (save-restriction
        (widen)
        (while (and (not parent-task) (org-up-heading-safe))
          (when (member (nth 2 (org-heading-components)) org-todo-keywords-1)
            (setq parent-task (point))))
        (if parent-task
            (org-with-point-at parent-task
              (org-clock-in))
          (when bh/keep-clock-running
            (bh/clock-in-default-task)))))))

(defvar bh/organization-task-id "87df8580-034c-4140-9cb9-e2638bf652e1")

(defun bh/clock-in-organization-task-as-default ()
  (interactive)
  (org-with-point-at (org-id-find bh/organization-task-id 'marker)
    (org-clock-in '(16))))

(defun bh/clock-out-maybe ()
  (when (and bh/keep-clock-running
             (not org-clock-clocking-in)
             (marker-buffer org-clock-default-task)
             (not org-clock-resolving-clocks-due-to-idleness))
    (bh/clock-in-parent-task)))

(add-hook 'org-clock-out-hook 'bh/clock-out-maybe 'append)

(defvar jb-agenda-block--refile
  '(tags "REFILE"
         ((org-agenda-overriding-header "Tasks to Refile")
          (org-tags-match-list-sublevels nil)))
  "A block showing the tasks I need to refile somewhere else")

(defvar jb-agenda-block--today
  '(agenda "" ((org-agenda-overriding-header "Today's Schedule:")
               (org-agenda-span 'day)
               (org-agenda-start-on-weekday nil)
               (org-agenda-start-day "+0d")))
  "A block showing my schedule for today")

(defvar jb-agenda-block--stuck-projects
  '(tags-todo "-CANCELLED/!"
              ((org-agenda-overriding-header "Stuck Projects")
               (org-agenda-skip-function 'bh/skip-non-stuck-projects)
               (org-agenda-sorting-strategy '(category-keep))))
  "A block showing projects with no NEXT child")

(defvar jb-agenda-block--active-projects
  '(tags-todo "-HOLD-CANCELLED/!"
              ((org-agenda-overriding-header "Active Projects")
               (org-agenda-skip-function 'bh/skip-non-projects)
               (org-tags-match-list-sublevels 'indented)
               (org-agenda-sorting-strategy '(category-keep))))
  "A block showing projects that aren't on hold or cancelled")

(defvar jb-agenda-block--next-tasks
  '(tags-todo "-CANCELLED/!NEXT"
              ((org-agenda-overriding-header "Project Next Tasks")
               (org-agenda-skip-function 'bh/skip-projects-and-habits-and-single-tasks)
               (org-tags-match-list-sublevels t)
               (org-agenda-sorting-strategy '(todo-state-down effort-up category-keep))
               (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
               (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
               (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)))
  "A block showing the next tasks for my projects")

(defvar jb-agenda-block--project-tasks
  '(tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
              ((org-agenda-overriding-header "Project Subtasks")
               (org-agenda-skip-function 'bh/skip-non-project-tasks)
               (org-agenda-sorting-strategy '(category-keep))
               (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
               (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
               (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)))
  "A block showing subtasks for my projects")


(defvar jb-agenda-block--waiting-or-on-hold
  '(tags-todo "-CANCELLED+WAITING|HOLD/!"
              ((org-agenda-overriding-header "Waiting or On Hold")
               (org-agenda-skip-function 'bh/skip-non-tasks)
               (org-tags-match-list-sublevels nil)
               (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
               (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
               (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)))
  "A block showing tasks waiting on something or put on hold")

(defvar jb-agenda-block--standalone-tasks
  '(tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
              ((org-agenda-overriding-header "Standalone Tasks")
               (org-agenda-skip-function 'bh/skip-project-tasks)
               (org-agenda-sorting-strategy '(category-keep))
               (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
               (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
               (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)))
  "A block showing tasks not belonging to a project")

(defvar jb-agenda-block--archive
  '(tags "-REFILE/"
         ((org-agenda-overriding-header "Tasks to Archive")
          (org-agenda-skip-function 'bh/skip-non-archivable-tasks)
          (org-tags-match-list-sublevels nil)))
  "A block showing tasks I need to archive")

(defvar jb-agenda-block--end-of-agenda
  '(tags "ENDOFAGENDA"
         ((org-agenda-overriding-header "End of Agenda")))
  "A block to mark the end of my agenda")

(defvar jb-org-agenda-display-settings
  '((org-agenda-start-with-log-mode nil)
    (org-agenda-log-mode-items '(clock))
    (org-agenda-tags-column 'auto)
    (org-agenda-time-grid (quote
                           ((daily today remove-match)
                            (800 1000 1200 1400 1600 1800)
                            "......" "----------------")))
    (org-agenda-todo-ignore-deadlines 'near)
    (org-agenda-todo-ignore-scheduled t))
  "Display settings for my agenda views.")

(after! org
  (setq org-agenda-custom-commands
        `(("N" "Notes" tags "NOTE"
                 ((org-agenda-overriding-header "Notes")
                  (org-tags-match-list-sublevels t)))
                (" " "Agenda"
                 (,jb-agenda-block--today
                  ,jb-agenda-block--refile
                  ,jb-agenda-block--next-tasks
                  ,jb-agenda-block--active-projects
                  ,jb-agenda-block--stuck-projects
                  ,jb-agenda-block--standalone-tasks
                  ,jb-agenda-block--end-of-agenda) ,jb-org-agenda-display-settings)
                ("r " "Agenda Review (All)"
                 (,jb-agenda-block--refile
                  ,jb-agenda-block--next-tasks
                  ,jb-agenda-block--standalone-tasks
                  ,jb-agenda-block--active-projects
                  ,jb-agenda-block--project-tasks
                  ,jb-agenda-block--waiting-or-on-hold
                  ,jb-agenda-block--archive
                  ,jb-agenda-block--end-of-agenda) ,jb-org-agenda-display-settings)
                ("rA" "Agenda Review (Archive)"
                 (,jb-agenda-block--archive
                  ,jb-agenda-block--end-of-agenda) ,jb-org-agenda-display-settings)
                ("rP" "Agenda Review (Projects)"
                 (,jb-agenda-block--active-projects
                  ,jb-agenda-block--next-tasks
                  ,jb-agenda-block--stuck-projects
                  ,jb-agenda-block--project-tasks
                  ,jb-agenda-block--end-of-agenda) ,jb-org-agenda-display-settings)
                ("rw" "Agenda Review (Waiting or On Hold)"
                 (,jb-agenda-block--waiting-or-on-hold
                  ,jb-agenda-block--end-of-agenda) ,jb-org-agenda-display-settings)
                )))

(defun org-columns--summary-apply-date (fun dates)
  "Apply fun to Dates."
  (format-time-string "<%Y-%m-%d %a>"
                      (seconds-to-time
                       (apply fun
                              (mapcar #'org-time-string-to-seconds dates)))))
(defun org-columns--summary-max-date (dates _)
  "Max Dates."
  (org-columns--summary-apply-date #'max dates))
(defun org-columns--summary-min-date (dates _)
  "Max Dates."
  (org-columns--summary-apply-date #'min dates))
(setq org-columns-summary-types '(
                                  ("dmax" . org-columns--summary-max-date)
                                  ("dmin" . org-columns--summary-min-date)))
(after! org
  (org-clock-persistence-insinuate)
  (setq org-columns-default-format "%80ITEM(Task) %10Effort(Effort){:} %10CLOCKSUM"
        org-global-properties (quote (("Effort_ALL" . "0:15 0:30 0:45 1:00 2:00 3:00 4:00 5:00 6:00 0:00")))
        org-clock-history-length 23
        org-clock-in-resume t
        org-clock-in-switch-to-state 'bh/clock-in-to-next
        org-clock-into-drawer t
        org-clock-out-remove-zero-time-clocks t
        org-clock-out-when-done t
        org-clock-persist t
        org-clock-persist-query-resume nil
        org-clock-auto-clock-resolution (quote when-no-clock-is-running)
        org-clock-report-include-clocking-task t)

   (setq org-time-stamp-rounding-minutes (quote (1 1)))

   (setq org-agenda-clock-consistency-checks
         (quote (:max-duration "4:00"
                 :min-duration 0
                 :max-gap 0
                 :gap-ok-around ("4:00")))))

(after! org
  (setq org-tag-alist (quote ((:startgroup)
                              ("@home" . ?H)
                              ("@uni"  . ?u)
                              ("@errand" . ?e)
                              (:endgroup)
                              ("WAITING" . ?w)
                              ("HOLD" . ?h)
                              ("PERSONAL" . ?P)
                              ("UNI" . ?U)
                              ("PC" . ?p)
                              ("NOTE" . ?n)
                              ("CANCELLED" . ?c)
                              ("FLAGGED" . ??))))
  (setq org-fast-tag-selection-single-key (quote expert))
  (setq org-agenda-tags-todo-honor-ignore-options t)
  )

(after! org
  (setq org-archive-mark-done nil
        org-archive-location "~/cloud/org/archive/%s_archive::"))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (ipython . t)
   (gnuplot . t)))
(setq org-confirm-babel-evaluate nil)
(setq org-export-use-babel nil)
(add-hook 'org-babel-after-execute-hook 'org-display-inline-images 'append)

(map! :leader
      :prefix "n"
      "c" #'org-capture)
(map! "<f12>" #'org-agenda)
(map! "<f5>" #'org-todo)
(map! "<f9> I" #'bh/punch-in)
(map! "<f9> O" #'bh/punch-out)

(use-package org-ref
    :config
    (setq
         org-ref-completion-library 'org-ref-ivy-cite
         org-ref-get-pdf-filename-function 'org-ref-get-pdf-filename-helm-bibtex
         org-ref-default-bibliography (list "~/cloud/org/bibliography.bib")
         org-ref-bibliography-notes "~/cloud/org/bibnotes.org"
         org-ref-note-title-format (concat "* TODO %y - %t\n"
                                           ":PROPERTIES:\n"
                                           ":Custom_ID: %k\n"
                                           ":NOTER_DOCUMENT: %F\n"
                                           ":ROAM_KEY: cite:%k\n"
                                           ":AUTHOR: %9a\n"
                                           ":JOURNAL: %j\n"
                                           ":YEAR: %y\n"
                                           ":VOLUME: %v\n"
                                           ":PAGES: %p\n"
                                           ":DOI: %D\n"
                                           ":URL: %U\n"
                                           ":END:\n\n")
         org-ref-notes-directory "~/cloud/org/roam/lit/"
         org-ref-notes-function 'orb-edit-notes
         org-ref-pdf-directory "~/cloud/org/pdfs/"
    ))

(after! org-ref
  (setq
 bibtex-completion-library-path "~/cloud/org/pdfs/"
 bibtex-completion-notes-path "~/cloud/org/roam/lit/"
 bibtex-completion-bibliography "~/cloud/org/bibliography.bib"
 bibtex-completion-pdf-field "file"
 bibtex-completion-notes-template-multiple-files
 (concat
  "#+TITLE: ${title}\n"
  "#+ROAM_KEY: cite:${=key=}\n"
  "* TODO Notes\n"
  ":PROPERTIES:\n"
  ":Custom_ID: ${=key=}\n"
  ":NOTER_DOCUMENT: %(orb-process-file-field \"${=key=}\")\n"
  ":AUTHOR: ${author-abbrev}\n"
  ":JOURNAL: ${journaltitle}\n"
  ":DATE: ${date}\n"
  ":YEAR: ${year}\n"
  ":DOI: ${doi}\n"
  ":URL: ${url}\n"
  ":END:\n\n"
  ))
)

(use-package org-noter
  :after (:any org pdf-view)
  :config
  (setq
   ;; The WM can handle splits
   org-noter-notes-window-location 'other-frame
   ;; Please stop opening frames
   org-noter-always-create-frame nil
   ;; I want to see the whole file
   org-noter-hide-other nil
   ;; Everything is relative to the main notes file
   org-noter-notes-search-path (list "~/cloud/org/roam/lit/")
   )
  )

(require 'ox-taskjuggler)

(use-package! org-checklist)

; Erase all reminders and rebuilt reminders for today from the agenda
(defun bh/org-agenda-to-appt ()
  (interactive)
  (setq appt-time-msg-list nil)
  (org-agenda-to-appt))

; Rebuild the reminders everytime the agenda is displayed
(add-hook 'org-agenda-finalize-hook 'bh/org-agenda-to-appt 'append)

; This is at the end of my .emacs - so appointments are set up when Emacs starts
(bh/org-agenda-to-appt)

; Activate appointments so we get notifications
(appt-activate t)

; If we leave Emacs running overnight - reset the appointments one minute after midnight
(run-at-time "24:01" nil 'bh/org-agenda-to-appt)

(setq org-agenda-include-diary nil
      org-agenda-diary-file "~/cloud/org/diary.org")

(setq org-roam-db-location "~/cloud/org/roam/org-roam.db")
(setq org-roam-directory "~/cloud/org/roam")
(setq org-roam-index-file (concat org-roam-directory "/" "roam-home.org"))

(setq org-roam-capture-templates
        '(
          ("d" "default" plain (function org-roam-capture--get-point)
        "%?"
        :file-name "%(format-time-string \"notes/%Y-%m-%d--%H-%M-%SZ--${slug}\" (current-time) t)"
        :head "#+TITLE: ${title}
#+DATE: %T
#+HUGO_SLUG: ${slug}
#+ROAM_TAGS: "
        :unnarrowed t)
          ("t" "topic" plain (function org-roam-capture--get-point)
           "%?"
           :file-name "topic/${slug}"
           :head "#+TITLE: ${title}
#+DATE: %T
#+HUGO_SLUG: ${slug}
#+ROAM_TAGS: "
           :unnarrowed t)
        ))

(after! org-roam-server
  :config
  (setq org-roam-server-host "127.0.0.1"
        org-roam-server-port 8080
        org-roam-server-authenticate nil
        org-roam-server-export-inline-images t
        org-roam-server-serve-files nil
        org-roam-server-served-file-extensions '("pdf" "mp4" "ogv")
        org-roam-server-network-poll t
        org-roam-server-network-arrows nil
        org-roam-server-network-label-truncate t
        org-roam-server-network-label-truncate-length 60
        org-roam-server-network-label-wrap-length 20))
(use-package! org-roam-protocol
  :after org-protocol)

(use-package! org-roam-bibtex
  :after (org-roam)
  :hook (org-roam-mode . org-roam-bibtex-mode)
  :config
  (setq org-roam-bibtex-preformat-keywords
        '("=key=" "title" "url" "file" "author-or-editor" "keywords"))
  (setq orb-templates
        `(("r" "ref" plain (function org-roam-capture--get-point)
           ""
           :file-name "lit/${slug}"
           :head ,(concat
                   "#+title: ${=key=}: ${title}\n"
                   "#+roam_key: ${ref}\n\n"
                   "* ${title}\n"
                   "  :PROPERTIES:\n"
                   "  :Custom_ID: ${=key=}\n"
                   "  :URL: ${url}\n"
                   "  :AUTHOR: ${author-or-editor}\n"
                   "  :NOTER_DOCUMENT: %(orb-process-file-field \"${=key=}\")\n"
                   "  :NOTER_PAGE: \n"
                   "  :END:\n")
           :unnarrowed t))))

(use-package company-org-roam
  :after org-roam
  :config
  (set-company-backend! 'org-mode '(company-org-roam
                                    company-yasnippet
                                    company-dabbrev)))

(setq message-send-mail-function 'smtpmail-send-it
      mu4e-maildir  "~/.local/mail/"
      mu4e-change-filenames-when-moving t ;; Fix the UID duplicate errors
      mu4e-view-prefer-html t
      mu4e-html2text-command "w3m -T text/html"
      mu4e-get-mail-command "mbsync -a -c '/home/jb/.config/isync/mbsyncrc'"
      mu4e-mu-home "~/.config/mu"
      mu4e-attachment-dir "~/.local/mail/attachments"
      mu4e-view-show-addresses t
      )
(after! mu4e
  (setq mu4e-bookmarks
        '(( :name "Unread Messages"
            :key ?u
            :query "flag:unread AND NOT flag:trashed AND NOT maildir:/personal/Archive AND NOT maildir:/university/Archive")
          ( :name "Today"
            :key ?t
            :query "date:today..now AND NOT maildir:/personal/Archive AND NOT maildir:/university/Archive")
          ( :name "Archive"
            :key ?a
            :query "maildir:/personal/Archive OR maildir:/university/Archive")))
  (setq mu4e-contexts
        `( ,(make-mu4e-context
             :name "jonnobrow.co.uk"
             :enter-func (lambda () (mu4e-message "Entering jonnobrow.co.uk context"))
          :leave-func (lambda () (mu4e-message "Leaving jonnobrow.co.uk context"))
          :match-func (lambda (msg)
                        (when msg
                          (string-match-p "^/personal" (mu4e-message-field msg :maildir))))
          :vars '( (mu4e-sent-folder         . "/personal/Sent")
                   (mu4e-drafts-folder       . "/personal/Drafts")
                   (mu4e-trash-folder        . "/personal/Trash")
                   (mu4e-refile-folder       . "/personal/Archive")
                   (mu4e-view-show-addresses . t)
                   (smtpmail-smtp-user       . "jonathan@jonnobrow.co.uk")
                   (smtpmail-smtp-server     . "smtp.mailbox.org")
                   (smtpmail-local-domain    . "jonnobrow.co.uk")
                   (smtpmail-smtp-service    . 587)
                   (smtpmail-stream-type     . starttls))
          )
           ,(make-mu4e-context
             :name "soton.ac.uk"
             :enter-func (lambda () (mu4e-message "Entering uni context"))
          :leave-func (lambda () (mu4e-message "Leaving uni context"))
          :match-func (lambda (msg)
                        (when msg
                          (string-match-p "^/university" (mu4e-message-field msg :maildir))))
          :vars '( (mu4e-sent-folder         . "/university/Sent Items")
                   (mu4e-drafts-folder       . "/university/Drafts")
                   (mu4e-trash-folder        . "/university/Deleted Items")
                   (mu4e-refile-folder       . "/university/Archive")
                   (mu4e-view-show-addresses . t)
                   (smtpmail-smtp-user       . "jsb1g18@soton.ac.uk")
                   (smtpmail-smtp-server     . "smtp.office365.com")
                   (smtpmail-smtp-service    . 587)
                   (smtpmail-stream-type     . starttls))
          ))))

(mu4e-alert-set-default-style 'libnotify)
(add-hook! 'after-init-hook #'mu4e-alert-enable-notifications)
(add-hook! 'after-init-hook #'mu4e-alert-enable-mode-line-display)

(setq reftex-default-bibliography '("~/cloud/org/bibliography.bib"))
(setq org-latex-pdf-process (list "latexmk -shell-escape -bibtex -f -pdf %f"))

(use-package! treemacs
  :defer t
  :config
  (setq treemacs-file-event-delay 1000
        treemacs-indentation 2
        treemacs-show-hidden-files nil
        treemacs-width 50
        treemacs-persist-file "~/.config/doom/treemacs-persist.org"))

(use-package deft
  :after org
  :bind ("C-c n d" . deft)
  :custom
  (deft-recursive t)
  (deft-use-filter-string-for-filename t)
  (deft-default-extension "org")
  (deft-directory "~/cloud/org/roam"))

(setq diary-file "~/cloud/documents/diary")

(defun jb/appt-display-libnotify (min-to-app new-time msg)
  (jb/send-notification
   (format "Appointment in %s minutes" min-to-app)
   (format "%s" msg)))

(defun jb/send-notification (title msg)
  (let ((notifier-path (executable-find "notify-send")))
    (start-process
     "Appointment Alert"
     "*Appointment Alert*"
     notifier-path
     title
     msg
     "-a" "Emacs"
     "-i" "emacs")))

(after! appt
  (setq appt-time-msg-list nil
        appt-display-interval '2
        appt-message-warning-time '10
        appt-display-mode-line nil
        appt-display-format 'window
        appt-disp-window-function (function jb/appt-display-libnotify)))
