[{
    // -------------------------------------------------------------------------
    // Global configuration
    // -------------------------------------------------------------------------

    "layer": "top",
    "position": "top",
    // If height property would be not present, it'd be calculated dynamically
    "height": 30,
    "modules-left": [
        "pulseaudio",
        "idle_inhibitor"
    ],
    "modules-center": [
	      "custom/pacman",
        "tray"
    ],
    "modules-right": [
        "network",
        "memory",
        "cpu"
    ],


    // -------------------------------------------------------------------------
    // Modules
    // -------------------------------------------------------------------------

    "battery": {
        "interval": 10,
        "states": {
            "warning": 30,
            "critical": 15
        },
        // Connected to AC
        "format": "  {icon}  {capacity}%", // Icon: bolt
        // Not connected to AC
        "format-discharging": "{icon}  {capacity}%",
        "format-icons": [
            "", // Icon: battery-full
            "", // Icon: battery-three-quarters
            "", // Icon: battery-half
            "", // Icon: battery-quarter
            ""  // Icon: battery-empty
        ],
        "tooltip": true
    },

    

    "cpu": {
        "interval": 5,
        "format": " {usage}% ({load})", // Icon: microchip
        "states": {
          "warning": 70,
          "critical": 90
        }
    },

    "memory": {
        "interval": 5,
        "format": " {}%", // Icon: memory
        "states": {
            "warning": 70,
            "critical": 90
        }
    },

    "network": {
        "interval": 5,
        "format-wifi": "{essid}", // Icon: wifi
        "format-ethernet": " {ifname}:{ipaddr}/{cidr}", // Icon: ethernet
        "format-disconnected": "⚠  Disconnected",
        "tooltip-format": "{ifname}: {ipaddr}"
    },

    "sway/mode": {
        "format": "<span style=\"italic\">  {}</span>", // Icon: expand-arrows-alt
        "tooltip": false
    },

    "sway/window": {
        "format": "{}",
        "max-length": 30
    },

    "pulseaudio": {
        //"scroll-step": 1,
        "format": " {icon} {volume}% ",
        "format-bluetooth": " {icon} {volume}% ",
        "format-muted": "",
        "format-icons": {
            "headphones": "",
            "handsfree": "",
            "headset": "",
            "phone": "",
            "portable": "",
            "car": "",
            "default": ["", ""]
        },
        "on-click": "pavucontrol"
    },

    "temperature": {
      "critical-threshold": 80,
      "interval": 5,
      "format": "{icon}  {temperatureC}°C",
      "format-icons": [
          "", // Icon: temperature-empty
          "", // Icon: temperature-quarter
          "", // Icon: temperature-half
          "", // Icon: temperature-three-quarters
          ""  // Icon: temperature-full
      ],
      "tooltip": true
    },

    "tray": {
        "icon-size": 21,
        "spacing": 10
    },

    "custom/pacman": {
        "format": "{} ",
        "interval": 60,                      // every minute
        "exec": "checkupdates | wc -l",      // num of updates
        "exec-if": "exit 0",                 // always run; consider advanced run conditions
        "on-click": "kitty sudo pacman -Syu; pkill -SIGRTMIN+8 waybar", // update system
        "signal": 8
    },

    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    }
},
{
    "layer": "top",
    "position": "bottom",
    "modules-left": ["sway/workspaces",
                     "sway/mode"],
    "modules-center": ["sway/window"],
    "modules-right": [
                     "clock#date",
                     "clock#time"],
    "sway/window": {
        "max-length": 50
    },

    "clock#time": {
        "interval": 1,
        "format": "{:%H:%M:%S}",
        "tooltip": false
    },

    "clock#date": {
      "interval": 10,
      "format": " {:%e %b %Y}", // Icon: calendar-alt
      "tooltip-format": "{:%e %B %Y}"
    }
}]